# Finger-driven cell diffusion

## Description
This code is part of the submitted work:
 
 "A variational model for finger-driven cell diffusion in the extra-cellular matrix". Favata, A., Rodella, A., Vidoli, S.

## Installation
dolfinx v0.6.0
https://github.com/FEniCS/dolfinx/tree/v0.6.0?tab=readme-ov-file#docker-images