from imp import lock_held
from mpi4py import MPI
import json as js
from pathlib import Path


def generate_a_quarter_hollow_mesh(
    a, r, lc, tdim, refinement_ratio=10, msh_file=None, comm=MPI.COMM_WORLD, alg=5
):
    # For further documentation see
    # - gmsh tutorials, e.g. see https://gitlab.onelab.info/gmsh/gmsh/-/blob/master/tutorial/python/t10.py
    # - dolfinx-gmsh interface https://github.com/FEniCS/dolfinx/blob/master/python/demo/gmsh/demo_gmsh.py

    facet_tag_names = {"hole1": 101, "right": 11, "top": 12, "left": 13, "bottom": 14}
    tag_names = {"facets": facet_tag_names}

    if comm.rank == 0:
        import gmsh

        # Initialise gmsh and set options
        gmsh.initialize()
        gmsh.option.setNumber("General.Terminal", 1)
        gmsh.option.setNumber("Mesh.Algorithm", alg)
        gmsh.model.mesh.optimize("Netgen")
        model = gmsh.model()
        model.add("Rectangle")
        model.setCurrent("Rectangle")

        r1 = r
        l = a * r

        pc = model.geo.addPoint(0, 0, 0, lc, tag=0)
        p1 = model.geo.addPoint(r1, 0, 0, lc, tag=1)
        p2 = model.geo.addPoint(0, r1, 0, lc, tag=2)
        p3 = model.geo.addPoint(l, 0, 0, lc)
        p4 = model.geo.addPoint(l, l, 0, lc)
        p5 = model.geo.addPoint(0, l, 0, lc)

        c_int1 = model.geo.addCircleArc(p2, pc, p1, tag=101)

        l1 = model.geo.addLine(p1, p3, tag=14)
        l2 = model.geo.addLine(p3, p4, tag=11)
        l3 = model.geo.addLine(p4, p5, tag=12)
        l4 = model.geo.addLine(p5, p2, tag=13)
        qloop = model.geo.addCurveLoop([c_int1, l1, l2, l3, l4])
        surface = model.geo.addPlaneSurface([qloop])

        model.mesh.field.add("Distance", 1)
        model.mesh.field.setNumbers(1, "NodesList", [pc])  # , pT])

        #
        # SizeMax -                     /------------------
        #                              /
        #                             /
        #                            /
        # SizeMin -o----------------/
        #          |                |    |
        #        Point         DistMin  DistMax

        model.mesh.field.add("Threshold", 2)
        model.mesh.field.setNumber(2, "IField", 1)
        model.mesh.field.setNumber(2, "LcMin", lc / refinement_ratio)
        model.mesh.field.setNumber(2, "LcMax", lc)
        model.mesh.field.setNumber(2, "DistMin", 3 * r1)
        model.mesh.field.setNumber(2, "DistMax", 5 * r1)
        model.mesh.field.setAsBackgroundMesh(2)

        model.geo.synchronize()
        surface_entities = [model[1] for model in model.getEntities(tdim)]
        model.addPhysicalGroup(tdim, surface_entities, tag=20)
        model.setPhysicalName(tdim, 20, "Rectangle surface")

        for k, v in facet_tag_names.items():
            gmsh.model.addPhysicalGroup(tdim - 1, [v], tag=v)
            gmsh.model.setPhysicalName(tdim - 1, v, k)

        model.mesh.generate(tdim)

        # Optional: Write msh file
        if msh_file is not None:
            os.makedirs(os.path.dirname(msh_file), exist_ok=True)
            gmsh.write(msh_file)
            with open(Path(msh_file).parent.joinpath("tag_names.json"), "w") as f:
                js.dump(tag_names, f)

    if comm.rank == 0:
        model_out = gmsh.model
    else:
        model_out = 0

    return model_out, tdim, tag_names


if __name__ == "__main__":
    import sys
    import os

    sys.path.append("../../damage")
    from dolfinx.io import XDMFFile
    from dolfinx.io.gmshio import model_to_mesh
    from mpi4py import MPI

    comm = MPI.COMM_WORLD

    outdir = "tryout"
    outname = "hollow_mesh"
    if comm.rank == 0:
        Path(outdir).mkdir(parents=True, exist_ok=True)
        prefix = os.path.join(outdir, outname)

    tdim = 2

    gmsh_model1, tdim, tag_indices1 = generate_a_quarter_hollow_mesh(
        10, 0.1, 0.03, tdim, refinement_ratio=2
    )

    mesh1, cell_tags, mts1 = model_to_mesh(gmsh_model1, comm, 0, gdim=tdim)

    with XDMFFile(comm, f"{prefix}.xdmf", "w", encoding=XDMFFile.Encoding.HDF5) as file:
        file.write_mesh(mesh1)
        file.write_meshtags(cell_tags)
        file.write_meshtags(mts1)
