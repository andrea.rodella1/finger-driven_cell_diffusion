#!/usr/bin/env python3
import numpy as np
import yaml
from pathlib import Path
import sys
import os
from mpi4py import MPI
import petsc4py
from petsc4py import PETSc
import dolfinx.plot
from dolfinx.common import list_timings
from dolfinx import log
from dolfinx.io import XDMFFile
from dolfinx.io.gmshio import model_to_mesh
from dolfinx.fem import (
    Function,
    FunctionSpace,
    locate_dofs_geometrical,
    locate_dofs_topological,
    dirichletbc,
    set_bc,
    Expression,
)

import ufl

sys.path.append("..")
from src.utils import ColorPrint
from src.models import ChemoAnisElasticityModel as ChemoMechModel
from src.alternate_minimization import (
    AltMinChemoElasticitySolver as ChemoElasticitySolver,
)
from mesh.mesh import generate_a_quarter_hollow_mesh

petsc4py.init(sys.argv)
log.set_log_level(log.LogLevel.WARNING)


comm = MPI.COMM_WORLD

with open("parameters.yml") as f:
    parameters = yaml.load(f, Loader=yaml.FullLoader)

# Get mesh parameters
tdim = parameters["model"]["model_dimension"]

# Get directory and name
outdir = parameters["output"]["outdir"]
outname = parameters["output"]["outname"]

if comm.rank == 0:
    Path(outdir).mkdir(parents=True, exist_ok=True)
prefix = os.path.join(outdir, outname)

# Create the mesh of the specimen with given dimensions
gmsh_model1, tdim, tag_indices1 = generate_a_quarter_hollow_mesh(
    10, 0.1, 0.02, tdim, refinement_ratio=6
)

# Get mesh and meshtags
mesh1, cell_tags, mts1 = model_to_mesh(gmsh_model1, comm, 0, gdim=tdim)


def all(mesh, mts, tag_indices, prefix):
    with XDMFFile(comm, f"{prefix}.xdmf", "w", encoding=XDMFFile.Encoding.HDF5) as file:
        file.write_mesh(mesh)

    # Measures
    dx = ufl.Measure("dx", domain=mesh)

    # Function spaces
    element_u = ufl.VectorElement("Lagrange", mesh.ufl_cell(), degree=2, dim=tdim)
    V_u = FunctionSpace(mesh, element_u)
    V_ux = FunctionSpace(mesh, ufl.FiniteElement("Lagrange", mesh.ufl_cell(), degree=2))
    V_uy = V_u.sub(1).collapse()[0]

    element_alpha = ufl.FiniteElement("Lagrange", mesh.ufl_cell(), degree=1)
    V_alpha = FunctionSpace(mesh, element_alpha)

    element_mu = ufl.FiniteElement("Lagrange", mesh.ufl_cell(), degree=1)
    V_mu = FunctionSpace(mesh, element_mu)

    V_theta = FunctionSpace(mesh, ("Discontinuous Lagrange", 0))

    # Define the state
    u = Function(V_u, name="Displacement")
    zero_ux_ = Function(V_ux, name="Zero Boundary Displacement in x")
    zero_uy_ = Function(V_uy, name="Zero Boundary Displacement in y")

    mu = Function(V_mu, name="ChemicalPotential")
    mun = Function(V_mu, name="ChemicalPotentialOld")
    un = Function(V_u, name="DisplacementOld")
    Dmu = Function(V_mu, name="fixed_mu_bc")
    Dmu.interpolate(
        lambda x: parameters["model"]["chemical"]["Dmu"] * np.ones_like(x[0])
    )

    alpha = Function(V_alpha, name="Anisotropy")

    theta = Function(V_theta, name="Orientation")
    theta.interpolate(lambda x: np.pi / 2 + np.arctan2(x[1], x[0]))

    dtime = Function(V_mu, name="time")

    state = {
        "u": u,
        "mu": mu,
        "alpha": alpha,
        "un": un,
        "mun": mun,
        "dtime": dtime,
        "theta": theta,
    }

    # need upper/lower bound for the Anisotropy field
    alpha_lb = Function(V_alpha, name="Lower bound")
    alpha_ub = Function(V_alpha, name="Upper bound")

    dofs_ux_left = locate_dofs_geometrical(
        (V_u.sub(0), V_ux), lambda x: np.isclose(x[0], 0)
    )
    dofs_uy_bottom = locate_dofs_geometrical(
        (V_u.sub(1), V_uy), lambda x: np.isclose(x[1], 0)
    )

    # Set Bcs Function
    zero_ux_.interpolate(lambda x: np.zeros_like(x[0]))
    zero_uy_.interpolate(lambda x: np.zeros_like(x[1]))
    alpha_lb.interpolate(lambda x: np.zeros_like(x[0]))
    alpha_ub.interpolate(lambda x: np.ones_like(x[0]))
    hole1 = mts.indices[mts.values == tag_indices["facets"]["hole1"]]
    dofs_mu_hole1 = locate_dofs_topological(V_mu, tdim - 1, np.array(hole1))

    for f in [
        alpha_lb,
        alpha_ub,
        Dmu,
    ]:
        f.vector.ghostUpdate(
            addv=PETSc.InsertMode.INSERT, mode=PETSc.ScatterMode.FORWARD
        )

    bcs_u = [
        dirichletbc(zero_ux_, dofs_ux_left, V_u.sub(0)),
        dirichletbc(zero_uy_, dofs_uy_bottom, V_u.sub(1)),
    ]

    bcs_mu = [
        dirichletbc(Dmu, dofs_mu_hole1),
    ]

    ds = ufl.Measure("ds", domain=mesh, subdomain_data=mts)
    bcs_alpha = []

    set_bc(alpha_ub.vector, bcs_alpha)

    alpha_ub.vector.ghostUpdate(
        addv=PETSc.InsertMode.INSERT, mode=PETSc.ScatterMode.FORWARD
    )

    bcs = {
        "bcs_u": bcs_u,
        "bcs_mu": bcs_mu,
        "bcs_alpha": bcs_alpha,
    }
    # Define the model
    # external mechanical work
    p = Function(V_u)
    p.interpolate(lambda x: (np.ones_like(x[0]), np.ones_like(x[1])))
    p.vector.ghostUpdate(addv=PETSc.InsertMode.INSERT, mode=PETSc.ScatterMode.FORWARD)
    external_work = ufl.dot(p, state["u"]) * ds(101)
    # Energy functional
    model = ChemoMechModel(state, parameters["model"])
    total_energy = model.total_energy_density(state) * dx
    total_energy -= external_work
    # Energy balance weak form
    weak_energy_balance = model.energy_balance(state) * dx

    Nincr = 200
    loads = np.logspace(1, 2, Nincr + 1)
    ploads = np.concatenate((np.linspace(0, 8, 5), np.linspace(8, 20, 200 - 5)[1:]))

    alternate_minimization = ChemoElasticitySolver(
        total_energy,
        weak_energy_balance,
        state,
        bcs,
        parameters.get("model"),
        parameters.get("solvers"),
        bounds=(alpha_lb, alpha_ub),
    )

    for i_t, t in enumerate(np.diff(loads)):
        # update the lower bound
        alpha.vector.copy(alpha_lb.vector)
        alpha_lb.vector.ghostUpdate(
            addv=PETSc.InsertMode.INSERT, mode=PETSc.ScatterMode.FORWARD
        )
        # update the time intervall
        dtime.interpolate(lambda x: t * np.ones_like(x[0]))
        dtime.vector.ghostUpdate(
            addv=PETSc.InsertMode.INSERT, mode=PETSc.ScatterMode.FORWARD
        )
        # update the pressure bc
        p.interpolate(
            lambda x: (
                -30 * ploads[i_t] * np.cos(np.arctan2(x[1], x[0])),
                -30 * ploads[i_t] * np.sin(np.arctan2(x[1], x[0])),
            )
        )
        p.vector.ghostUpdate(
            addv=PETSc.InsertMode.INSERT, mode=PETSc.ScatterMode.FORWARD
        )

        ColorPrint.print_bold(f"-- Solving for t = {i_t:3.2f} --")

        alternate_minimization.solve()

        element_ten = ufl.TensorElement("Lagrange", mesh.ufl_cell(), 1, shape=(3, 3))
        V_out = FunctionSpace(mesh, element_ten)
        epsilon = Function(V_out, name="Strain")
        sigma = Function(V_out, name="Stress")
        eps_exp = Expression(model.eps(u), V_out.element.interpolation_points())
        epsilon.interpolate(eps_exp)
        sig_exp = Expression(
            model.stress(model.eps(u), state), V_out.element.interpolation_points()
        )
        sigma.interpolate(sig_exp)
        theta_out = Function(theta.function_space, name="Orientation[DEG]")
        th_exp = Expression(theta * 180 / np.pi, V_theta.element.interpolation_points())
        theta_out.interpolate(th_exp)

        with XDMFFile(
            comm, f"{prefix}.xdmf", "a", encoding=XDMFFile.Encoding.HDF5
        ) as file:
            file.write_function(u, t)
            file.write_function(mu, t)
            file.write_function(alpha, t)
            file.write_function(epsilon, t)
            file.write_function(sigma, t)
            file.write_function(theta_out, t)

        if i_t == 100:
            break

    return state


state1 = all(mesh1, mts1, tag_indices1, prefix)
