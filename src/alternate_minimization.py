from mpi4py import MPI
import sys
import os
from petsc4py import PETSc
import petsc4py
import ufl
import dolfinx
import dolfinx.common
from dolfinx.fem import (
    Expression,
    Function,
)
from src.utils import ColorPrint

from src.petsc_solvers import SNESSolver, TAOSolver
from src.utils import ColorPrint

petsc4py.init(sys.argv)
dir_path = os.path.dirname(os.path.realpath(__file__))


class AltMinChemoElasticitySolver:
    def __init__(
        self,
        total_energy,
        weak_energy_balance,
        state,
        bcs,
        model_parameters={},
        solver_parameters={},
        bounds=(dolfinx.fem.function.Function, dolfinx.fem.function.Function),
        variable_names=["u", "mu"],
        stabilizing_term=None,
        monitor=None,
        comm=MPI.COMM_WORLD,
    ):
        self.comm = comm
        self.variable_names = variable_names
        self.stabilizing_term = stabilizing_term
        self.u = state[self.variable_names[0]]
        self.un = state["un"]
        self.mu = state[self.variable_names[1]]
        self.mun = state["mun"]
        self.bcs = bcs
        self.dtime = state["dtime"]

        self.u_old = dolfinx.fem.Function(self.u.function_space)
        self.u.vector.copy(result=self.u_old.vector)
        self.mu_old = dolfinx.fem.Function(self.mu.function_space)
        self.mu.vector.copy(result=self.mu_old.vector)

        self.dtime_old = dolfinx.fem.Function(self.dtime.function_space)
        self.dtime.vector.copy(result=self.dtime.vector)

        self.u_old.x.scatter_forward()
        self.mu_old.x.scatter_forward()
        self.dtime_old.x.scatter_forward()

        self.alpha = state["alpha"]
        V_alpha = self.alpha.function_space
        self.alpha_old = dolfinx.fem.Function(V_alpha)
        self.alpha.vector.copy(result=self.alpha_old.vector)
        self.alpha_old.x.scatter_forward()

        self.theta = state["theta"]

        self.total_energy = total_energy
        self.alpha_lb = bounds[0]
        self.alpha_ub = bounds[1]

        self.model_parameters = model_parameters
        self.solver_parameters = solver_parameters

        self.monitor = monitor
        V_u = self.u.function_space

        energy_u = ufl.derivative(self.total_energy, self.u, ufl.TestFunction(V_u))

        energy_mu = weak_energy_balance

        energy_alpha = ufl.derivative(
            self.total_energy, self.alpha, ufl.TestFunction(V_alpha)
        )

        self.F = [energy_u, energy_mu, energy_alpha]

        self.elasticity = SNESSolver(
            energy_u,
            self.u,
            bcs.get("bcs_u"),
            bounds=None,
            petsc_options=self.solver_parameters.get("elasticity").get("snes"),
            prefix=self.solver_parameters.get("elasticity").get("prefix"),
        )

        self.chemo = SNESSolver(
            energy_mu,
            self.mu,
            bcs.get("bcs_mu"),
            bounds=None,
            petsc_options=self.solver_parameters.get("chemo").get("snes"),
            prefix=self.solver_parameters.get("chemo").get("prefix"),
        )

        if self.solver_parameters.get("anisotropy").get("type") == "SNES":
            self.anisotropy = SNESSolver(
                energy_alpha,
                self.alpha,
                bcs.get("bcs_alpha"),
                bounds=(self.alpha_lb, self.alpha_ub),
                petsc_options=self.solver_parameters.get("anisotropy").get("snes"),
                prefix=self.solver_parameters.get("anisotropy").get("prefix"),
            )

        if self.solver_parameters.get("anisotropy").get("type") == "TAO":
            self.anisotropy = TAOSolver(
                total_energy,
                self.alpha,
                bcs.get("bcs_alpha"),
                bounds=(self.alpha_lb, self.alpha_ub),
                petsc_options=self.solver_parameters.get("anisotropy").get("tao"),
                prefix=self.solver_parameters.get("anisotropy").get("prefix"),
            )

    def stop_criterion(self, parameters=None):
        """Stop criterion for alternate minimization"""

        if parameters is None:
            parameters = self.solver_parameters.get("anisotropy_elasticity")

        criterion = parameters.get("criterion")

        if criterion == "energy":
            criterion_error_energy_r = self.error_energy_r <= parameters.get(
                "energy_rtol"
            )
            criterion_error_energy_a = self.error_energy_a <= parameters.get(
                "energy_atol"
            )
            return criterion_error_energy_r or criterion_error_energy_a

        raise RuntimeError(f"Wrong criterion. it be 'energy' instead of: {criterion}")

    def eig_vecmat(self):
        u = self.u
        T = ufl.sym(ufl.grad(u))
        tol = 1.0e-18
        TI = (ufl.tr(T) + ufl.sqrt(ufl.tr(T) ** 2 - 4 * ufl.det(T))) / 2
        TII = (ufl.tr(T) - ufl.sqrt(ufl.tr(T) ** 2 - 4 * ufl.det(T))) / 2

        Txx, Txy, Tyy = T[0, 0], T[0, 1], T[1, 1]

        theta_I = ufl.atan_2(TI - Txx, Txy)
        theta_II = theta_I + ufl.pi / 2

        vI = ufl.conditional(
            ufl.gt(abs(Txy), tol),
            ufl.as_vector([ufl.cos(theta_I), ufl.sin(theta_I)]),
            ufl.as_vector([0.0, 1.0]),
        )
        vII = ufl.conditional(
            ufl.gt(abs(Txy), tol),
            ufl.as_vector([ufl.cos(theta_II), ufl.sin(theta_II)]),
            ufl.as_vector([1.0, 0.0]),
        )
        return theta_II

    def solve(self, monitor=None, parameters=None):
        if parameters is None:
            parameters = self.solver_parameters.get("anisotropy_elasticity")

        self.max_it = parameters.get("max_it")

        self.total_energy_int_old = 0

        self.alpha.vector.copy(result=self.alpha_old.vector)
        self.alpha_old.x.scatter_forward()

        self.u.vector.copy(result=self.u_old.vector)
        self.mu.vector.copy(result=self.mu_old.vector)
        self.u_old.x.scatter_forward()
        self.mu_old.x.scatter_forward()

        self.dtime.vector.copy(result=self.dtime_old.vector)
        self.dtime_old.x.scatter_forward()

        for iteration in range(self.max_it):
            ColorPrint.print_color("~Alternate Minimization : Elasticity solver")
            with dolfinx.common.Timer("~Alternate Minimization : Elastic solver"):
                (self.solver_u_it, self.solver_u_reason) = self.elasticity.solve()

            ############# problem in theta ################
            thetanew = Function(self.theta.function_space)
            theta_exp = Expression(
                self.eig_vecmat(),
                self.theta.function_space.element.interpolation_points(),
            )
            thetanew.interpolate(theta_exp)
            thetanew.vector.copy(result=self.theta.vector)
            self.theta.x.scatter_forward()
            ###############################################

            ColorPrint.print_warn("~Alternate Minimization : Anisotropy solver")
            with dolfinx.common.Timer("~Alternate Minimization : Anisotropy solver"):
                (
                    self.solver_alpha_it,
                    self.solver_alpha_reason,
                ) = self.anisotropy.solve()
            ColorPrint.print_pass("~Alternate Minimization : Chemo solver")
            with dolfinx.common.Timer("~Alternate Minimization : Chemo solver"):
                (self.solver_u_it, self.solver_u_reason) = self.chemo.solve()

            # compute errors
            self.total_energy_int = self.comm.allreduce(
                dolfinx.fem.assemble_scalar(dolfinx.fem.form(self.total_energy)),
                op=MPI.SUM,
            )
            self.error_energy_a = abs(self.total_energy_int - self.total_energy_int_old)

            if self.total_energy_int_old > 0:
                self.error_energy_r = abs(
                    self.total_energy_int / self.total_energy_int_old - 1
                )
            else:
                self.error_energy_r = 1.0

            # update
            self.total_energy_int_old = self.total_energy_int

            # monitors
            if self.monitor is not None:
                self.monitor()

            if monitor is not None:
                monitor(iteration=iteration, data=self.data)

            ColorPrint.print_info(
                f"AM - Iteration: {iteration:3d}, "
                + f"energy_r: {self.error_energy_r:3.4e}, "
                + f"energy_a: {self.error_energy_a:3.4e}"
            )
            # check convergence
            if self.stop_criterion(parameters=parameters):
                break

        else:
            if not parameters.get("error_on_nonconvergence"):
                ColorPrint.print_warn(
                    (
                        f"Could not converge after {iteration:3d} iterations,"
                        + f"energy_r: {self.error_energy_r:3.4e}, "
                        + f"energy_a: {self.error_energy_a:3.4e}"
                    )
                )

        # update un and mun
        self.u.vector.copy(self.un.vector)
        self.mu.vector.copy(self.mun.vector)
        self.un.x.scatter_forward()
        self.mun.x.scatter_forward()

        # update pGmu
        self.dtime_old.vector.copy(self.dtime.vector)
        self.dtime.x.scatter_forward()
