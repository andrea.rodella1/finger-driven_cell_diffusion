import ufl


class ElasticityModel:
    # Basic class for elasticity

    def __init__(self, model_parameters={}, eps0=None):
        self.model_parameters = model_parameters

        """
        Initializes the sound material parameters.
        * Sound material parameters:
            - model_parameters["E"]: sound Young modulus
            - model_parameters["nu"]: sound Poisson ratio
        """
        # Sound material paramaters
        self.E = self.model_parameters["E"]
        self.nu = self.model_parameters["nu"]
        self.model_dimension = self.model_parameters["model_dimension"]
        self.model_type = self.model_parameters["model_type"]
        self.lmbda = (
            self.E
            * self.nu
            / ((1 + self.nu) * (1 - (self.model_dimension - 1) * self.nu))
        )
        self.mu = self.E / (2 * (1 + self.nu))

    def eps(self, u):
        if self.model_type == "2D":
            return ufl.sym(ufl.grad(u))
        if self.model_type == "plane-strain":
            return ufl.sym(
                ufl.as_matrix(
                    [
                        [u[0].dx(0), u[0].dx(1), 0],
                        [u[1].dx(0), u[1].dx(1), 0],
                        [0, 0, 0],
                    ]
                )
            )

    def elastic_energy_density_strain(self, eps):
        """
        Returns the elastic energy density from the strain variables.
        """
        # Parameters
        lmbda = self.lmbda
        mi = self.mi
        # Elastic energy density
        return 1 / 2 * (2 * mi * ufl.inner(eps, eps) + lmbda * ufl.tr(eps) ** 2)

    def elastic_energy_density(self, state):
        """
        Returns the elastic energy density from the state.
        """
        # Compute the elastic strain tensor
        strain = self.eps(state["u"])
        # Elastic energy density
        return self.elastic_energy_density_strain(strain)

    def total_energy_density(self, state):
        """
        Returns the total energy density calculated from the state.
        """
        # Calculate total energy density
        return self.elastic_energy_density(state)


class ChemoAnisElasticityModel(ElasticityModel):
    """
    Base class for transversely isotropic elasticity coupled with diffusion.
    """

    def __init__(self, state, model_parameters={}):
        """
        Initializes the sound material parameters.
            - lmbda: 1st lamé modulus
            - mi: isothermal shear modulus
            - kappa: bulk modulus kappa=lmbda+2/3*mi
            - beta: stress-chemical modulus beta=-3*kappa*achem
            - mis: isotropic chemical conductivity
            - man: anisotropic chemical conductivity
            - c: chemistry modulus
        """
        # Initialize the elastic parameters
        super().__init__(model_parameters)
        self.E = self.model_parameters["chemical"]["E"]
        self.nu = self.model_parameters["chemical"]["nu"]
        self.dim = self.model_dimension
        self.achem = self.model_parameters["chemical"]["achem"]
        self.mis = self.model_parameters["chemical"]["mis"]
        self.man = self.model_parameters["chemical"]["man"]
        self.c = self.model_parameters["chemical"]["c"]
        self.T0 = self.model_parameters["chemical"]["T0"]
        self.lmbda = (
            self.E
            * self.nu
            / ((1 + self.nu) * (1 - (self.model_dimension - 1) * self.nu))
        )
        self.mi = self.E / (2 * (1 + self.nu))
        self.kappa = self.lmbda + 2 / 3 * self.mi
        self.beta = -3 * self.kappa * self.achem

        self.ell = self.model_parameters["ell"]
        self.wa_pl = self.model_parameters["diss_terms"]["wa_pl"]
        self.wa_mn = self.model_parameters["diss_terms"]["wa_mn"]
        self.wad = (self.wa_pl - self.wa_mn) / 2

    def normal(self, state):
        """
        Returns the normal vector wrt the fibre's family orientation
        """
        theta = state["theta"]
        n = ufl.as_vector([ufl.cos(theta), ufl.sin(theta), 0])
        return n

    def w(self, alpha):
        """
        Return the dissipated energy function as a function of the state
        (only depends on anisotropy).
        """
        return alpha

    def gradmu(self, mu):
        return ufl.as_vector([mu.dx(0), mu.dx(1), 0])

    def Malpha(self, state):
        alpha = state["alpha"]
        n = self.normal(state)
        mis = self.mis
        man = self.man
        Id = ufl.Identity(3)
        M = mis * Id + alpha * man * ufl.outer(n, n)
        return M

    def q(self, state):
        mis = self.mis
        mu = state["mu"]
        q = -mis * self.gradmu(mu)
        return q

    def energy_density(self, state):
        """
        Returns the elastic energy density from the strain and the anisotropy.
        """
        # Parameters
        mu = state["mu"]
        u = state["u"]
        eps = self.eps(u)
        Malpha = self.Malpha(state)
        beta = self.beta
        E = self.E
        nu = self.nu
        alpha = state["alpha"]
        n = self.normal(state)
        En = ufl.dot(eps, n)
        en_density = E / 2 / (1 - pow(nu, 2)) * (
            (1 - nu) * (ufl.inner(eps, eps))
            + nu * pow(ufl.tr(eps), 2)
            - (1 - pow(nu, 2)) * alpha * pow(ufl.dot(En, n), 2)
        ) + beta * mu * ufl.tr(eps)
        en_density += ufl.dot(ufl.dot(Malpha, self.gradmu(mu)), self.gradmu(mu))
        return en_density

    def damage_dissipation_density(self, state):
        """
        Return the anisotropy dissipation density from the state.
        """
        # Get the material parameters
        wad = self.wad
        ell = self.ell
        # Get the anisotropy
        alpha = state["alpha"]
        # Compute the anisotropy gradient
        grad_alpha = ufl.grad(alpha)
        # Compute the anisotropy dissipation density
        D_d = wad * self.w(alpha) + wad * ell**2 * ufl.dot(grad_alpha, grad_alpha) / 2
        return D_d

    def total_energy_density(self, state):
        """
        Return the total energy density from the state.
        """
        energy = self.energy_density(state)
        dissipation = self.damage_dissipation_density(state)
        return energy + dissipation

    def energy_balance(self, state):
        T0 = self.T0
        c = self.c
        beta = self.beta
        mis = self.mis
        man = self.man
        alpha = state["alpha"]
        mu = state["mu"]
        mun = state["mun"]
        u = state["u"]
        un = state["un"]
        dtime = state["dtime"]
        n = self.normal(state)
        dmu = ufl.TestFunction(mu.function_space)
        grad_mu = self.gradmu(mu)
        grad_dmu = self.gradmu(dmu)
        en_bal = (
            c * (mu - mun) / dtime * dmu
            - beta * T0 * ufl.tr(self.eps(u - un)) / dtime * dmu
            + mis * ufl.dot(grad_mu, grad_dmu)
            + alpha * man * ufl.dot(grad_mu, n) * ufl.dot(grad_dmu, n)
        )
        return en_bal

    def elastic_energy_density_strain(self, eps, state):
        """
        Returns the elastic energy density from the strain.
        """
        # Parameters
        E = self.E
        nu = self.nu
        beta = self.beta

        alpha = state["alpha"]
        mu = state["mu"]
        n = self.normal(state)
        En = ufl.dot(eps, n)

        freeS = E / 2 / (1 - pow(nu, 2)) * (
            (1 - nu) * (ufl.inner(eps, eps))  # inner eps_{ij}*eps_{ij}
            + nu * pow(ufl.tr(eps), 2)
            - (1 - pow(nu, 2)) * alpha * pow(ufl.dot(En, n), 2)
        ) + beta * mu * ufl.tr(eps)
        return freeS

    def stress(self, strain, state):
        # Differentiate the elastic energy w.r.t. the strain tensor
        eps_ = ufl.variable(strain)
        # Derivative of energy w.r.t. the strain tensor to obtain the stress
        # tensor
        sig = ufl.diff(self.elastic_energy_density_strain(eps_, state), eps_)
        return sig
